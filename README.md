# Perfect Square

### What is the project?
The project is a combination of 2 functionalities in an application. The project works on the concept of perfect squares. I used angular to make the application.

### Functionalities:-
##### Home
This is a basic page which the user will be redirected at the start. It contains the basic overview of the project. 

![Home Page](/readme images/Home.png)

##### Perfect Sqaure Check:
This part takes a numeric input from the user and checks whether the given number is a perfect square or not, and display the result.

![Perfect Square Check](/readme images/Check.png)

##### Perfect Sqaure Range:
This part takes 2 numeric inputs: an upper bound and a lower bound. Once the numbers are taken, the application will print all the numbers which are perfect square lying in between the range and also show their roots. 

![Perfect Square Check](/readme images/Range.png)

### Constraints:
Each part has majorly 2 constraints. 
1. The input has to be given
2. The input cannot be negative

Along with this there is an additional constraint on the second application which states that
1. Lower bound should always be lower than Upper bound.


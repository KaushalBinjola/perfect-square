import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PerfectCheckService {
  constructor() { }

  checkPerfect(num:number):boolean | number{
    if(num === 1){
      return 1
    }
    if(num <=3){
      return false
    }
    for(let i = 2; i<=num/2;i++){
      if(i**2===num){
        return i
      }
      continue;
    }
    return false
  }

}

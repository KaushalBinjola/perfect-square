import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NumberValidators } from '../number.validators';
import { PerfectCheckService } from '../perfect-check.service';

@Component({
  selector: 'app-perfect-range',
  templateUrl: './perfect-range.component.html',
  styleUrls: ['./perfect-range.component.css']
})
export class PerfectRangeComponent {
  output:string[] =["These are the Numbers and their roots:-"]
  toShow: boolean = false

  form = new FormGroup({
    number1: new FormControl('',[
      Validators.required,
      NumberValidators.notNegative
    ]),
    number2: new FormControl('',[
      Validators.required,
      NumberValidators.notNegative
    ])
  })

  constructor(private perfCheck:PerfectCheckService) { }

  onSubmit(){
    for(let i=0; i<=this.output.length;i++){
      this.output.pop()
    }
    this.output.push("These are the Numbers and their roots:-")
    let lowerNum = this.form.get('number1')?.value
    let upperNum = this.form.get('number2')?.value
    if(lowerNum>=upperNum){
      this.form.setErrors({
        num1Greater:true
      })
    }
    else{
      this.form.setErrors({
        num1Greater:false
      })
    }
    
    let lowerNumRoot
    let upperNumRoot

    for(let i = lowerNum;i<=upperNum;i++){
      let result = this.perfCheck.checkPerfect(i) 
      if(result){
        lowerNumRoot = result as number
        break;
        // console.log(`Number: ${i}, Root: ${result}`)
      }
      else{
        continue;
      }
    }
    for(let i = upperNum;i>=lowerNum;i--){
      let result = this.perfCheck.checkPerfect(i) 
      if(result){
        upperNumRoot = result as number
        break;
        // console.log(`Number: ${i}, Root: ${result}`)
      }
      else{
        continue;
      }
    }
    if(upperNumRoot&&lowerNumRoot){
      for(let i = lowerNumRoot;i<=upperNumRoot;i++){
        // console.log(`Number: ${i**2}, Root: ${i}`)
        this.output.push(`Number: ${i**2}, Root: ${i}`)
      }
      console.log(this.output)
    }
    if(this.output.length == 1){
      this.output.splice(0,1,"Sorry no perfect squares in range :(")
    }
    this.toShow = true
  }

  get number1(){
    return this.form.get('number1')
  }

  get number2(){
    return this.form.get('number2')
  }

}

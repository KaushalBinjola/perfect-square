import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfectRangeComponent } from './perfect-range.component';

describe('PerfectRangeComponent', () => {
  let component: PerfectRangeComponent;
  let fixture: ComponentFixture<PerfectRangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfectRangeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfectRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

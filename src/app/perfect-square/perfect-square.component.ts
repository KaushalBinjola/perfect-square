import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NumberValidators } from '../number.validators';
import { PerfectCheckService } from '../perfect-check.service';

@Component({
  selector: 'app-perfect-square',
  templateUrl: './perfect-square.component.html',
  styleUrls: ['./perfect-square.component.css']
})

export class PerfectSquareComponent {
  toShow = false
  log = ""

  form= new FormGroup({
    number1 : new FormControl('',[
      Validators.required,
      NumberValidators.notNegative
    ])
  })
  constructor(private perfCheck: PerfectCheckService) {
  }

  submit() {
    let number = this.form.get('number1')?.value
    let result = this.perfCheck.checkPerfect(number)
    if (result) {
      this.log = `The number is a perfect square and its root is ${result}`
      this.toShow = true
      console.log(this.toShow)
    }
    else {    
      this.log = `The number is not a perfect square `
      this.toShow = true
    }
  }

  get number1(){
    return this.form.get('number1')
  }
}

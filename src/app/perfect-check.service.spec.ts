import { TestBed } from '@angular/core/testing';

import { PerfectCheckService } from './perfect-check.service';

describe('PerfectCheckService', () => {
  let service: PerfectCheckService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PerfectCheckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

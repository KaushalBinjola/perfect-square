import { AbstractControl, ValidationErrors } from "@angular/forms";

export class NumberValidators{
    static notNegative(control:AbstractControl):ValidationErrors|null{
        if(control.value < 0){
            return {notNegative:true}
        }
        return null
    }
}